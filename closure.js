const counter = () => {
  let count = 0;
  return () => ++count;
};

const counter1 = counter();
console.log(counter1());
console.log(counter1());
console.log(counter1());

const counter2 = counter();
console.log(counter2());
console.log(counter2());
console.log(counter2());
